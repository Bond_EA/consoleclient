﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace ConsoleClient
{
    class Util
    {
        public static bool SocketSimpleConnected(Socket tcpHandler)
        {
            return !((tcpHandler.Poll(1000, SelectMode.SelectRead) && (tcpHandler.Available == 0)) || !tcpHandler.Connected);
        }

        public static string GetRemoteIp(Socket tcpHandler)
        {
            string rawRemoteIP = tcpHandler.RemoteEndPoint.ToString();
            int dotsIndex = rawRemoteIP.LastIndexOf(":");
            string remoteIP = rawRemoteIP.Substring(0, dotsIndex);
            return remoteIP;
        }
        public static string GetRemoteIpAndPort(Socket tcpHandler)
        {
            return tcpHandler.RemoteEndPoint.ToString();
        }

        public static void ManageString(string str, Client client)
        {
            if (str.StartsWith("|"))
            {
                string[] stringArray = str.Split('|');

                /*
                  tcp|bro
                  hello there
                  udp|gggg            
                 */

                bool sendTcp = false;
                bool sendUdp = false;

                for (int i = 0; i < stringArray.Length; i++)
                {
                    if (stringArray[i].Equals("tcp"))
                    {
                        sendTcp = true;
                        sendUdp = false;
                    }
                    if (stringArray[i].Equals("udp"))
                    {
                        sendTcp = false;
                        sendUdp = true;
                    }
                }

                string pureMessage = str.Replace("tcp", "")
                    .Replace("udp", "")
                    .Replace("|", "");


                if (sendTcp)
                    client.SendMessageTcp(pureMessage);
                else client.SendMessageUdp(pureMessage);
            }
            else
            {
                client.SendMessageTcp(str);
            }
        }
    }
}
