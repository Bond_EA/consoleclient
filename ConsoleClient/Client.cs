﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;


namespace ConsoleClient
{
    class Client
    {
        //public static string address = "18.192.64.12";
        public static string address = "127.0.0.1";

        public static int portTcp = 8384; // portTcp
        public static int portUdp = 8385; // portUdp

        Socket tcpSocket;
        Socket udpSocket;

        public bool connected;

        public Client()
        {
            ConnectTo();
        }
        #region Get And Send Message ------------------------------------------------------------------
        public void HandleMessageTcp(string message)
        {
            if(!message.Equals(""))
            Console.WriteLine($"[TCP][FROM SERVER {Util.GetRemoteIp(tcpSocket)}]: {message}");
        }

        public void SendMessageTcp(string message)
        {
            if (connected)
            {
                byte[] data = Encoding.Unicode.GetBytes(message);
                tcpSocket.Send(data);

            }
            else
            {
                Console.WriteLine("Not connected but trying to send message!");
            }
        }

        public void SendMessageUdp(string message)
        {
            // --------------------------------------UDP--------------------------------------
            EndPoint remotePoint = new IPEndPoint(IPAddress.Parse(address), portUdp);
            byte[] data = Encoding.Unicode.GetBytes(message);
            udpSocket.SendTo(data, remotePoint); //remoteEpUdp
        }
        #endregion -------------------------------------------------------------------------------------
        IPEndPoint remoteEpTcp;
        IPEndPoint remoteEpUdp;
        public void ConnectTo()
        {
            try
            {
                remoteEpTcp = new IPEndPoint(IPAddress.Parse(address), portTcp);
                remoteEpUdp = new IPEndPoint(IPAddress.Any, portUdp);

                tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                //udpSocket.Bind(remoteEpUdp);
                
                // TCP
                tcpSocket.Connect(remoteEpTcp);
                string serverConnectedTo = tcpSocket.RemoteEndPoint.ToString();
                Console.WriteLine("Tcp socket connected to {0}", serverConnectedTo);
                connected = true;


                new ReaderThread(this, tcpSocket, udpSocket, remoteEpUdp).StartThreads();
                new ConnectionCheckThread(this, tcpSocket).StartThread();
                
                while (true)
                {
                    string str = Console.ReadLine();
                    if (str.Equals("break")) break;
                    Util.ManageString(str, this);
                    //SendMessageTcp(str);
                }

            }
            catch (SocketException se)
            {
                Console.WriteLine("SocketException : {0}", se.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected exception : {0}", e.ToString());
            }
        }
    }    
}

