﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ConsoleClient
{
    class ConnectionCheckThread
    {
        Socket socket;
        Client client;

        Thread thread;

        public ConnectionCheckThread(Client client, Socket socket)
        {
            this.client = client;
            this.socket = socket;
        }

        public void StartThread()
        {
            thread = new Thread(new ThreadStart(ThreadCheck));
            thread.Start();

        }

        void ThreadCheck()
        {
            while (true)
            {
                try
                {
                    Thread.Sleep(1000);
                    bool result = SocketSimpleConnected(socket);
                    client.connected = result;
                    if (!result)
                    {
                        Console.WriteLine("No connection. Breaking out from Checker");
                        socket.Shutdown(SocketShutdown.Both);
                        socket.Dispose();
                        break;
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine("Connection was broken");
                    client.connected = false;
                    break;
                }
            }
        }

        bool SocketSimpleConnected(Socket s)
        {
            return !((s.Poll(1000, SelectMode.SelectRead) && (s.Available == 0)) || !s.Connected);
        }
    }
}
