﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ConsoleClient
{
    class ReaderThread
    {
        Socket socketTcp;
        Socket listenSocketUdp;
        IPEndPoint remoteEpUdp;

        Client client;

        byte[] bytesArray = new byte[1024];
        string data;

        Thread threadReaderTcp;
        Thread threadReaderUdp;

        public ReaderThread(Client client, Socket tcpSocket, Socket listenSocketUdp, IPEndPoint remoteEpUdp)
        {
            this.client = client;
            this.socketTcp = tcpSocket;
            this.listenSocketUdp = listenSocketUdp;
            this.remoteEpUdp = remoteEpUdp;
        }

        public void StartThreads()
        {
            threadReaderTcp = new Thread(new ThreadStart(ThreadReadTcp));
            threadReaderTcp.Start();

            threadReaderUdp = new Thread(new ThreadStart(ThreadReadUdp));
            threadReaderUdp.Start();
        }

        void ThreadReadTcp()
        {
            while (true)
            {
                if (client.connected)
                {
                    try
                    {
                        // получаем ответ
                        bytesArray = new byte[1024]; // буфер для ответа
                        StringBuilder builder = new StringBuilder();
                        int bytes = 0; // количество полученных байт

                        do
                        {
                            bytes = socketTcp.Receive(bytesArray, bytesArray.Length, 0);
                            builder.Append(Encoding.Unicode.GetString(bytesArray, 0, bytes));
                        }
                        while (socketTcp.Available > 0);

                        client.HandleMessageTcp(builder.ToString());
                    }
                    catch (Exception e)
                    {
                        socketTcp.Shutdown(SocketShutdown.Both);
                        socketTcp.Dispose();
                        Console.WriteLine("Connection lost. It's either THE SERVER got shot down or this machine's internet connection was broken.");
                        break;
                        // Basically here I need to check, if server gets shot down while client is connected, we need to let him know that it's whether server got shut down connection broke.
                    }
                }
                else
                {
                    Console.WriteLine("No connection. Breaking out from Reader");
                    break;
                }
            }
        }

        #region Listen UPD - doesn't require established connection

        private void ThreadReadUdp()
        {
            try
            {
                listenSocketUdp.Bind(remoteEpUdp);
                byte[] data = new byte[1024];
                EndPoint remoteIp = new IPEndPoint(IPAddress.Any, Client.portUdp);

                StringBuilder builder = new StringBuilder();
                int bytes;
                while (true)
                {
                    do
                    {
                        bytes = listenSocketUdp.ReceiveFrom(data, ref remoteIp);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (listenSocketUdp.Available > 0);

                    IPEndPoint remoteFullIp = remoteIp as IPEndPoint;
                    Console.WriteLine($"[UDP][FROM SERVER {Util.GetRemoteIp(listenSocketUdp)}]: {builder}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " ||| "+ ex.StackTrace);
            }
            finally
            {
                CloseUdp();
            }
        }
        private void CloseUdp()
        {
            if (listenSocketUdp != null)
            {
                listenSocketUdp.Shutdown(SocketShutdown.Both);
                listenSocketUdp.Close();
                listenSocketUdp = null;
            }
        }
        #endregion

        string ReadLine(Socket reciever, byte[] buffer)
        {
            int bytesRec = reciever.Receive(buffer);
            string data = Encoding.ASCII.GetString(buffer, 0, bytesRec);
            return data;
        }

        bool SocketSimpleConnected(Socket s)
        {
            return !((s.Poll(1000, SelectMode.SelectRead) && (s.Available == 0)) || !s.Connected);
        }
    }
}
